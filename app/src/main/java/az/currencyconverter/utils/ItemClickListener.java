package az.currencyconverter.utils;

import android.view.View;

public interface ItemClickListener {

    void onItemClick(View v, int pos);

}
