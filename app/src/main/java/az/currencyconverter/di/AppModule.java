package az.currencyconverter.di;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;

import javax.inject.Named;
import javax.inject.Singleton;

import az.currencyconverter.data.LocalService;
import az.currencyconverter.data.model.DaoSession;
import dagger.Module;
import dagger.Provides;

@Module(includes = {NetworkModule.class,DatabaseModule.class})
public class AppModule {

    @Provides
    @Singleton
    public static LocalService createLocalService(@Named("currencyDaoSession") DaoSession daoSession, Gson gson) {
        return new LocalService(daoSession.getConvertItemDao(), gson);
    }

    @Provides
    static Context provideContext(Application application) {
        return application.getApplicationContext();
    }


}
