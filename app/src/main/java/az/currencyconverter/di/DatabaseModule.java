package az.currencyconverter.di;


import android.content.Context;

import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.query.QueryBuilder;

import javax.inject.Named;
import javax.inject.Singleton;

import az.currencyconverter.data.model.DaoMaster;
import az.currencyconverter.data.model.DaoSession;
import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    @Provides
    @Singleton
    @Named("currencyDaoSession")
    public DaoSession createDaoSession(Context context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "favorites-db");
        Database db = helper.getWritableDb();

        return new DaoMaster(db).newSession();
    }

}