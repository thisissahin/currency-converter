package az.currencyconverter.di;

import android.app.Application;

import javax.inject.Singleton;

import az.currencyconverter.BaseApp;
import az.currencyconverter.data.LocalService;
import az.currencyconverter.ui.favorites.FavoritesPresenter;
import az.currencyconverter.ui.home.HomePresenter;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class,
        AppModule.class})
public interface AppComponent extends AndroidInjector<BaseApp> {

    LocalService getLocalService();

    void inject(HomePresenter presenter);

    void inject(FavoritesPresenter presenter);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();

    }
}
