package az.currencyconverter.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import az.currencyconverter.R;
import az.currencyconverter.data.model.ConvertItem;
import az.currencyconverter.databinding.ItemFavoriteBinding;
import az.currencyconverter.utils.ItemClickListener;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {

    private ItemClickListener clickListener;
    private List<ConvertItem> dataList = new ArrayList<>();

    public FavoriteAdapter(ItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setDataList(List<ConvertItem> newList) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new FavoriteDiffUtil(dataList, newList));
        dataList = newList;
        diffResult.dispatchUpdatesTo(this);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFavoriteBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_favorite, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ConvertItem item = dataList.get(position);
        holder.bind(item);
        holder.binding.getRoot().setOnClickListener(v -> {
            clickListener.onItemClick(v,position);
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ItemFavoriteBinding binding;

        ViewHolder(@NonNull ItemFavoriteBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(ConvertItem item) {
            binding.setResult(item);
        }
    }
}
