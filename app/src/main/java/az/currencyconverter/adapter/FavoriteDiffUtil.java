package az.currencyconverter.adapter;

import java.util.List;

import az.currencyconverter.data.model.ConvertItem;

public class FavoriteDiffUtil extends androidx.recyclerview.widget.DiffUtil.Callback {
    private List<ConvertItem> oldList;
    private List<ConvertItem> newList;

    public FavoriteDiffUtil(List<ConvertItem> oldList, List<ConvertItem> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getKey().equals(oldList.get(newItemPosition).getKey());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).equals(oldList.get(newItemPosition));
    }
}
