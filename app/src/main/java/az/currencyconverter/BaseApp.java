package az.currencyconverter;

import android.app.Application;

import javax.inject.Inject;

import az.currencyconverter.di.AppComponent;
import az.currencyconverter.di.DaggerAppComponent;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public class BaseApp extends Application implements HasAndroidInjector {

    private static AppComponent appComponent;

    @Inject
    DispatchingAndroidInjector<Object> mActivityInjector;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return mActivityInjector;
    }
}
