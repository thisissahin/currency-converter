package az.currencyconverter.data.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConvertResponse {

    @SerializedName("rates")
    @Expose
    private JsonObject rates;

    public Double getConvertedAmount() {
        String key = rates.keySet().toArray()[0].toString();
        return rates.get(key).getAsJsonObject().get("rate_for_amount").getAsDouble();
    }

}