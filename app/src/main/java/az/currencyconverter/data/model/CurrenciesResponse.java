package az.currencyconverter.data.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrenciesResponse {

    @SerializedName("currencies")
    @Expose
    private JsonObject currencies;

  /*  public ArrayList<CurrencySearchItem> getCurrencies() {
        ArrayList<CurrencySearchItem> currencyList = new ArrayList<>();
        for (String it : currencies.keySet()) {
            currencyList.add(new CurrencySearchItem(it, currencies.get(it).getAsString()));
        }
        return currencyList;
    }*/

    public JsonObject getCurrencies() {
        return currencies;
    }
}