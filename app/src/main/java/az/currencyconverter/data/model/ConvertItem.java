package az.currencyconverter.data.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;

@Entity(indexes = {
        @Index(value = "firstCurrencyCode,secondCurrencyCode", unique = true)
})
public class ConvertItem {
    private String firstCurrencyCode = "AZN";
    private String firstCurrencyName = "Azerbaijani manat";
    private String secondCurrencyCode = "USD";
    private String secondCurrencyName = "United States dollar";
    @Id()
    private String key = firstCurrencyCode + secondCurrencyCode;


    @Generated(hash = 1253202117)
    public ConvertItem(String firstCurrencyCode, String firstCurrencyName,
                       String secondCurrencyCode, String secondCurrencyName, String key) {
        this.firstCurrencyCode = firstCurrencyCode;
        this.firstCurrencyName = firstCurrencyName;
        this.secondCurrencyCode = secondCurrencyCode;
        this.secondCurrencyName = secondCurrencyName;
        this.key = key;
    }

    @Generated(hash = 1909685208)
    public ConvertItem() {
    }


    public String getFirstCurrencyCode() {
        return firstCurrencyCode;
    }

    public void setFirstCurrencyCode(String firstCurrencyCode) {
        this.firstCurrencyCode = firstCurrencyCode;
        updateKey();
    }

    public String getSecondCurrencyCode() {
        return secondCurrencyCode;
    }

    public void setSecondCurrencyCode(String secondCurrencyCode) {
        this.secondCurrencyCode = secondCurrencyCode;
        updateKey();
    }

    public String getFirstCurrencyTitle() {
        return firstCurrencyCode + "(" + firstCurrencyName + ")";
    }

    public String getSecondCurrencyTitle() {
        return secondCurrencyCode + "(" + secondCurrencyName + ")";
    }

    public void updateKey() {
        key = firstCurrencyCode + secondCurrencyCode;
    }

    public void swapCurrencies() {
        String temp = firstCurrencyCode;
        firstCurrencyCode = secondCurrencyCode;
        secondCurrencyCode = temp;
        temp = firstCurrencyName;
        firstCurrencyName = secondCurrencyName;
        secondCurrencyName = temp;
        updateKey();
    }

    public String getFirstCurrencyName() {
        return this.firstCurrencyName;
    }

    public void setFirstCurrencyName(String firstCurrencyName) {
        this.firstCurrencyName = firstCurrencyName;
    }

    public String getSecondCurrencyName() {
        return this.secondCurrencyName;
    }

    public void setSecondCurrencyName(String secondCurrencyName) {
        this.secondCurrencyName = secondCurrencyName;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }


}
