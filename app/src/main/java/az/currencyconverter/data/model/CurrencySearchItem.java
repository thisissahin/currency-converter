package az.currencyconverter.data.model;

import ir.mirrajabi.searchdialog.core.Searchable;

public class CurrencySearchItem implements Searchable {

    private String currencyCode;
    private String currencyName;

    public CurrencySearchItem(String currencyCode, String currencyName) {
        this.currencyCode = currencyCode;
        this.currencyName = currencyName;
    }

    @Override
    public String getTitle() {
        return currencyCode + " " + "(" + currencyName + ")";
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }
}
