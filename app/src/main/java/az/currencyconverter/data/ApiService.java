package az.currencyconverter.data;

import java.util.Map;

import az.currencyconverter.data.model.ConvertResponse;
import az.currencyconverter.data.model.CurrenciesResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiService {

    @GET("currency/convert")
    Observable<ConvertResponse> convert(@QueryMap Map<String, String> queries);

    @GET("currency/list")
    Observable<CurrenciesResponse> getCurrencyList(@Query("api_key") String apiKey);
}
