package az.currencyconverter.data;

import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import az.currencyconverter.data.model.ConvertItem;
import az.currencyconverter.data.model.ConvertItemDao;

public class LocalService {

    private final ConvertItemDao convertItemDao;
    private final Gson gson;

    @Inject
    public LocalService(ConvertItemDao convertItemDao, Gson gson) {
        this.convertItemDao = convertItemDao;
        this.gson = gson;
    }

    public List<ConvertItem> getAllFavorites() {
        return this.convertItemDao.queryBuilder().listLazy();
    }

    //Burda item shallow copy oldugunu ucun problem cixirdi. Ona bu formada deep copy etdim.
    public void insertItem(ConvertItem item) {
        if (checkExist(item.getKey())) {
            convertItemDao.delete(item);
            return;
        }
        String json = gson.toJson(item);
        ConvertItem deepCopy = gson.fromJson(json,ConvertItem.class);
        convertItemDao.insertOrReplace(deepCopy);
    }

    public boolean checkExist(String key) {
        return convertItemDao.queryBuilder().where(ConvertItemDao.Properties.Key.eq(key)).list().size() > 0;
    }
}