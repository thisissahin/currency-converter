package az.currencyconverter.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.databinding.DataBindingUtil;

import az.currencyconverter.BaseApp;
import az.currencyconverter.R;
import az.currencyconverter.data.model.ConvertItem;
import az.currencyconverter.data.model.DaoSession;
import az.currencyconverter.databinding.ActivityMainBinding;
import moxy.MvpAppCompatActivity;

public class MainActivity extends MvpAppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
    }

}