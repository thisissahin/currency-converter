package az.currencyconverter.ui.home;

import java.util.List;

import az.currencyconverter.data.model.CurrencySearchItem;
import moxy.MvpView;
import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface HomeView extends MvpView {
    void getCurrencyList(List<CurrencySearchItem> list);
    void getConvertedAmount(Double amount);
    void showMessage(String message);
    void checkFavorite(boolean isFavorite);
}
