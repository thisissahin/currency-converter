package az.currencyconverter.ui.home;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import az.currencyconverter.R;
import az.currencyconverter.base.BaseFragment;
import az.currencyconverter.data.model.ConvertItem;
import az.currencyconverter.data.model.CurrencySearchItem;
import az.currencyconverter.databinding.FragmentHomeBinding;
import az.currencyconverter.utils.Utils;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import moxy.presenter.InjectPresenter;

public class HomeFragment extends BaseFragment<FragmentHomeBinding> implements HomeView {

    @InjectPresenter
    HomePresenter presenter;
    private ConvertItem convertItem = new ConvertItem();
    private boolean firstClicked = true;

    private SimpleSearchDialogCompat<CurrencySearchItem> currencyDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.checkFavorite(convertItem.getKey());
        getBinding().setConvertItem(convertItem);
        getBinding().amountInput.requestFocus();
        getBinding().convertButton.setOnClickListener(v -> {
            Utils.closeKeyboard(requireActivity());
            getBinding().loading.setVisibility(View.VISIBLE);
            presenter.convert(convertItem.getFirstCurrencyCode(), convertItem.getSecondCurrencyCode(),
                    Double.valueOf(getBinding().amountInput.getText().toString()));
        });

        getBinding().favoritesButton.setOnClickListener(v -> {
            getNavController().navigate(R.id.action_homeFragment_to_favoritesFragment);
        });

        getBinding().swapButton.setOnClickListener(v -> swapCurrencies());

        getBinding().favoriteButton.setOnClickListener(v -> presenter.insertFavorite(convertItem));

        getBinding().amountInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getBinding().convertButton.setEnabled(!charSequence.toString().isEmpty());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        MutableLiveData<String> liveData = Objects.requireNonNull(getNavController().getCurrentBackStackEntry())
                .getSavedStateHandle()
                .getLiveData("selected");
        liveData.observe(getViewLifecycleOwner(), json -> {
            if (!json.isEmpty()) {
                convertItem = new Gson().fromJson(json, ConvertItem.class);
                getBinding().setConvertItem(convertItem);
                presenter.checkFavorite(convertItem.getKey());
                liveData.setValue("");
            }
        });

    }

    private void swapCurrencies() {
        convertItem.swapCurrencies();
        presenter.checkFavorite(convertItem.getKey());
        getBinding().setConvertItem(convertItem);
    }

    @Override
    public void getCurrencyList(List<CurrencySearchItem> list) {
        currencyDialog = new SimpleSearchDialogCompat<>(requireContext(), getString(R.string.search),
                getString(R.string.what_you_look_for), null, new ArrayList<>(list),
                (dialog, item, position) -> {
                    if (firstClicked) {
                        convertItem.setFirstCurrencyCode(item.getCurrencyCode());
                        convertItem.setFirstCurrencyName(item.getCurrencyName());
                    } else {
                        convertItem.setSecondCurrencyCode(item.getCurrencyCode());
                        convertItem.setSecondCurrencyName(item.getCurrencyName());
                    }
                    getBinding().setConvertItem(convertItem);
                    currencyDialog.getSearchBox().setText("");
                    presenter.checkFavorite(convertItem.getKey());
                    Utils.closeKeyboard(requireActivity());
                    dialog.dismiss();
                });

        currencyDialog.setCancelOnTouchOutside(true);
        getBinding().firstCurrency.setOnClickListener(v -> {
            firstClicked = true;
            currencyDialog.show();
        });

        getBinding().secondCurrency.setOnClickListener(v -> {
            firstClicked = false;
            currencyDialog.show();
        });
    }

    @Override
    public void getConvertedAmount(Double amount) {
        getBinding().loading.setVisibility(View.INVISIBLE);
        getBinding().amount.setText(String.format(getString(R.string.money_format), amount, convertItem.getSecondCurrencyCode()));
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void checkFavorite(boolean isFavorite) {
        getBinding().setIsFavorite(isFavorite);
    }
}
