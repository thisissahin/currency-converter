package az.currencyconverter.ui.home;

import android.util.Log;
import android.util.Pair;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import az.currencyconverter.BaseApp;
import az.currencyconverter.base.BasePresenter;
import az.currencyconverter.data.ApiService;
import az.currencyconverter.data.LocalService;
import az.currencyconverter.data.model.ConvertItem;
import az.currencyconverter.data.model.CurrencySearchItem;
import az.currencyconverter.utils.Constants;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import moxy.InjectViewState;

@InjectViewState
public class HomePresenter extends BasePresenter<HomeView> {

    @Inject
    ApiService apiService;

    @Inject
    LocalService localService;

    public HomePresenter() {
        BaseApp.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getAllCountries();
    }

    public void insertFavorite(ConvertItem convertItem) {
        Log.d("TAG", "insertFavorite: ");
        localService.insertItem(convertItem);
        checkFavorite(convertItem.getKey());
    }

    public void checkFavorite(String key) {
        getViewState().checkFavorite(localService.checkExist(key));
    }

    public void convert(String from, String to, Double amount) {
        Map<String, String> queries = new HashMap<>();
        queries.put("api_key", Constants.API_KEY);
        queries.put("from", from);
        queries.put("to", to);
        queries.put("amount", amount.toString());

        if (hasInternet()) {
            getComposite().add(apiService.convert(queries)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(response -> {
                        getViewState().getConvertedAmount(response.getConvertedAmount());
                    }, error -> {
                        getViewState().showMessage(error.getLocalizedMessage());
                    }));
        } else {
            getViewState().showMessage("No Internet!");
        }
    }

    public void getAllCountries() {
        if (hasInternet()) {
            getComposite().add(apiService.getCurrencyList(Constants.API_KEY)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .flatMap(response -> Observable.fromIterable(response.getCurrencies().keySet())
                            .map(key -> new Pair<>(key, response.getCurrencies().get(key).getAsString())))
                    .flatMap(item -> Observable.just(new CurrencySearchItem(item.first, item.second)))
                    .toList()
                    .subscribe(items -> {
                        getViewState().getCurrencyList(items);
                    }, error -> {
                        getViewState().showMessage(error.getLocalizedMessage());
                    }));
        } else {
            getViewState().showMessage("No Internet!");
        }
    }

}
