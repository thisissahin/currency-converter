package az.currencyconverter.ui.favorites;

import java.util.List;

import az.currencyconverter.data.model.ConvertItem;
import moxy.MvpView;
import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface FavoritesView extends MvpView {

    void getFavorites(List<ConvertItem> list);

}
