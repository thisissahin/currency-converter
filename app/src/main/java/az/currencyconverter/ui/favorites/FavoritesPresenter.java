package az.currencyconverter.ui.favorites;

import com.google.gson.Gson;

import javax.inject.Inject;

import az.currencyconverter.BaseApp;
import az.currencyconverter.data.LocalService;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class FavoritesPresenter extends MvpPresenter<FavoritesView> {

    @Inject
    LocalService localService;

    @Inject
    Gson gson;

    public FavoritesPresenter() {
        BaseApp.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().getFavorites(localService.getAllFavorites());
    }
}
