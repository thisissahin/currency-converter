package az.currencyconverter.ui.favorites;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import az.currencyconverter.R;
import az.currencyconverter.adapter.FavoriteAdapter;
import az.currencyconverter.base.BaseFragment;
import az.currencyconverter.data.model.ConvertItem;
import az.currencyconverter.databinding.FragmentFavoritesBinding;
import az.currencyconverter.utils.ItemClickListener;
import moxy.presenter.InjectPresenter;

public class FavoritesFragment extends BaseFragment<FragmentFavoritesBinding> implements FavoritesView, ItemClickListener {

    @InjectPresenter
    FavoritesPresenter presenter;


    private FavoriteAdapter favoriteAdapter;
    private List<ConvertItem> convertItems;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_favorites;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBinding().toolbar.setNavigationOnClickListener(v -> getNavController().popBackStack());
        initRecyclerView();
    }

    private void initRecyclerView() {
        favoriteAdapter = new FavoriteAdapter(this);
        getBinding().recyclerView.setAdapter(favoriteAdapter);
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
    }

    @Override
    public void getFavorites(List<ConvertItem> convertItems) {
        this.convertItems = convertItems;
        Log.d("TAG", "getFavorites: " + new Gson().toJson(convertItems));
        favoriteAdapter.setDataList(convertItems);
    }

    @Override
    public void onItemClick(View v, int pos) {
        Objects.requireNonNull(getNavController().getPreviousBackStackEntry()).
                getSavedStateHandle().set("selected", new Gson().toJson(convertItems.get(pos)));
        getNavController().popBackStack();
    }
}
